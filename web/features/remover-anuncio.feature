#language: pt

Funcionalidade: Remover Anúncios
    Sendo um anunciante que possui um equipamento indesejado
    Quero remover esse anúncio
    Para que eu possa manter o meu Dashboard atualizado.

    Contexto: Login
        * Login com "mbarros@hotmail.com.br" e "pwd123"

    
    Cenario: Remover o anúncio
        Dado que aeu tenha um anuncio indesejado
            | thumb     | telecaster.jpg |
            | nome      | Telecaster     |
            | categoria | Cordas         |
            | preco     | 50             |
        Quando eu solicito a exclusão desse item
        E confirmo a exclusão
        Então não devo ver esse item no meu Dashboard

    Cenario: Desistir da exclusão
        Dado que aeu tenha um anuncio indesejado
            | thumb     | conga.jpg |
            | nome      | Conga     |
            | categoria | Outros         |
            | preco     | 100             |
        Quando eu solicito a exclusão desse item
        Mas não confirmo a solicitação
        Então esse item deve permanecer no meu Dashboard.