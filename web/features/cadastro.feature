#language: pt

Funcionalidade: Cadastro
    Sendo um músico que possui equipamentos musicais
    Quero fazer o meu cadastro no Rocklov
    Para que eu possa disponibilizá-los para locação.

    @cadastro
    Cenario: Fazer Cadastro
        Dado que acesso a página de cadastro
        Quando submeteto o seguinte formulário de cadastro:
            | nome          | email             | senha  |
            | Marco Antonio | mbarros@gmail.com | pwd456 |
        Então sou redirecionado para o dashboard.

    @excecao
    Esquema do Cenario: Tentativa de Cadastro
        Dado que acesso a página de cadastro
        Quando submeteto o seguinte formulário de cadastro:
            | nome         | email         | senha         |
            | <nome_input> | <email_input> | <senha_input> |
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | nome_input    | email_input       | senha_input | mensagem_output                  |
            |               | mbarros@gmail.com | pwd456      | Oops. Informe seu nome completo! |
            | Marco Antonio |                   | pwd456      | Oops. Informe um email válido!   |
            | Marco Antonio | mbarros*gmail.com | pwd456      | Oops. Informe um email válido!   |
            | Marco Antonio | mbarros@gmail.com |             | Oops. Informe sua senha secreta! |