#language: pt

Funcionalidade: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais

    @basico
    Cenario: Login do usuário
        Dado que acesso a página principal
        Quando submeto minhas credenciais com "mbarros@yahoo.com" e "pwd123"
        Então sou redirecionado para o dashboard.

    @Alt
    Esquema do Cenario: Tentar Logar
        Dado que acesso a página principal
        Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | email_input       | senha_input | mensagem_output                  |
            | mbarros@yahoo.com | pwd456      | Usuário e/ou senha inválidos.    |
            | mbarro@yahoo.com  | pwd123      | Usuário e/ou senha inválidos.    |
            | mbarros*yahoo.com | pwd123      | Oops. Informe um email válido!   |
            |                   | pwd123      | Oops. Informe um email válido!   |
            | mbarros@yahoo.com |             | Oops. Informe sua senha secreta! |