describe "POST /sessions" do
  context "login com sucesso" do
    before(:all) do
      payload = { email: "login@gmail.com", password: "pwd123" }
      @result = Sessions.new.login(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  #   examples = [
  #     {
  #       payload: { email: "mbarros@gmail.com", password: "pwd124" },
  #       code: 401,
  #       error: "Unauthorized",
  #     },
  #     {
  #       title: "senha incorreta",
  #       payload: { email: "404@gmail.com", password: "pwd124" },
  #       code: 401,
  #       error: "Unauthorized",
  #     },
  #     {
  #       title: "sem informar o email",
  #       payload: { email: "", password: "pwd124" },
  #       code: 412,
  #       error: "required email",
  #     },
  #     {
  #       title: "sem o campo email",
  #       payload: { password: "pwd124" },
  #       code: 412,
  #       error: "required email",
  #     },
  #     {
  #       title: "sem informar a senha",
  #       payload: { email: "404@gmail.com", password: "" },
  #       code: 412,
  #       error: "required password",
  #     },
  #     {
  #       title: "sem o campo senha",
  #       payload: { email: "404@gmail.com" },
  #       code: 412,
  #       error: "required password",
  #     },
  #   ]
  examples = Helpers::get_fixture("login")
  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Sessions.new.login(e[:payload])
      end

      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "valida id do usuário" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
