describe "POST /equipos/{equipo_id}/bookings" do
  before(:all) do
    payload = { email: "locatario@gmail.com", password: "pwd123" }
    result = Sessions.new.login(payload)
    @locatario_id = result.parsed_response["_id"]
  end

  context "solicitar locacao" do
    before(:all) do
      #Dado que "Marco" tem um "Fender Strato" para Locação
      result = Sessions.new.login({ email: "locador@gmail.com", password: "pwd123" })
      locador_id = result.parsed_response["_id"]

      fender = {
        thumbnail: Helpers::get_thumb("fender-sb.jpg"),
        name: "Fender Strato",
        category: "Cordas",
        price: 150,
      }
      MongoDB.new.remove_equipo(fender[:name], locador_id)

      result = Equipos.new.create(fender, locador_id)
      fender_id = result.parsed_response["_id"]

      #quano solicito a locação do Fender do Locador
      @result = Equipos.new.booking(fender_id, @locatario_id)
    end
    it "deve retornar 200" do
      expect(@result.code).to eql 200
    end
  end
end
